##### section 5

# 로또 추첨기를 통해 hooks 에서 useCallback과 useMemo 사용하기

> ## 로또 추첨기 컴포넌트

- 반복문을 사용해서 setTimeout을 표현함  
  class는 표현하기 쉬운데, hooks가 어려울 수 있음.

* 반복문을 기점으로 컴포넌트를 분리화 해준다.

* 보통, **최하단 자식 컴포넌트는 PureComponent화 한다(대부분은 화면구현이기때문)**  
  데이터게산기능이 없다면 함수 컴포넌트화 해도 무방( memo사용하기)

* 컴포넌트를 또다른 컴포넌트로 감싸는것 **HOC, 하이오더 컴포넌트** 라 함

> ## setTImeout 여러번 사용하기

- 시작하자마자 setTimeout이 실행 되기때문에 이때 componentDidMount()사용.  
  (보통 비동기에 변수사용하면 closure 문제가 생기지만 이때 **let**을 사용 할 것.)

```
componentDidMount(){
  const {winNumbers} = this.state;
  for(let i=0; i<=this.state.winNumbers.length-1;i++){
    setTimeout(()=>{
      this.setState( (prevState) =>{
        return{
        winBalls:[...prevState.winBalls,winNumbers[i]],
        };
      });
    }, (i+1)*1000));
  }
  setTimeout(()=>{
    this.setState({
      bonus:winNumbers[6],
      redo:true,
    });
  },7000);
}
```

- 부모에의해 컴포넌트가 없어질경우 , 메모리누수를 막기위해 setTimeout을 clear 해줘야한다.  
  componentWillUnmount()로 초기화를 해준다.

* setTimeout 을 clear하기 위해 this.timeouts로 할당해준다

> ## componentDidUpdate

- 초기화버튼 사용후 componentDidMount를 실행 하기위해 componentDidUpdate 작성  
  setState상황이 많아서 조건문으로 확실하게 DidUpdate를 판단 시켜야함.

```
//업데이트 하고 싶은 상황을 잘 처리해야함.
componentDidUpdate(prevProps,prevState){
  if(this.winBalls.length === 0){
    this.runTimeOuts(); // 중복되는 기능 함수로 빼기.
  }
}
```

> ## useEffect로 업데이트 감지하기

- Lotto에서 라이프사이클을 hooks로 변경시 성능문제가 발생 할 수있다. (이번강좌의 핵심)

* componentDidMount() {}  
  =>

```
useEffect(()=>{
    for(let i=0; i<=this.state.winNumbers.length-1;i++){
    timeouts.current[i] = setTimeout(()=>{
      setWinBalls((prevState)=>{
         [...prevState.winBalls,winNumbers[i]]);
      })
    }, (i+1)*1000));
  }
},[]) // input이 빈배열일땐 , **componentDidMount()와 같다.**
```

- componentDidUpdate() {}  
   =>
  ```
  useEffect(()=>{
    for(let i=0; i<=this.state.winNumbers.length-1;i++){
    timeouts.current[i] = setTimeout(()=>{
      setWinBalls((prevState)=>{
         [...prevState.winBalls,winNumbers[i]]);
      })
    }, (i+1)*1000));
  }
  },[]) // 배열에 요소가 있으면, componentDidMount와 componentDidUpdate를 **둘다** 수행 한다.
  ```

> ## useMemo와 useCallBack

- hooks의 특성상 함수 전체가 리렌더링 되기때문에 useState(getWinNumbers()); 가 재실행됨.  
  getWinNumbers()의 return 값은 숫자 6개 + 보너스 1개. **리턴값을 잠시 캐싱(저장)할 수 있는 useMemo가 있음**  
  {useMemo}from 'react' ;
  `const lottoNumbers = useMemo(()=>getWinNumbers(),[]);` =>  
  `const [winNumbers,setWinNumbers]=useState(**lottoNumbers**);`

  i.e) **useMemo: 복잡한 함수 결과값을 기억한다**  
  useRef: 일반 값을 기억 한다.

- 함수컴포넌트는 리렌더링 시 전체가 작동하기떄문에 함수작성에 비효율적이다.  
   그래서 useMemo를 활용하여 리턴값을 저장한다(inputs배열값이 변하기전까지)

* **리액트 활용중에는 함수마다 console.log 작성하여 원할때 실행되는지확인하는 버릇을 갖기**

* useMemo는 함수의**리턴**을 기억하고있지만, useCallback은 **함수**자체를 기억한다.  
  useCallback은 함수자체를 기억해둬서 , 함수컴포넌트가 재실행 되도 재생성 되지 않는다.  
  if) 함수자체의 볼륨이 매우크거나,계산이 오래 걸려서 성능의 문제가 생길떄 useCallback을 사용함

* useCallback을 사용할 때 문제 :  
  기억이 너무좋아서, 다른 당첨숫자를 보여주지 않음.  
  **useCallback으로 state값을 알고 싶으면 inputs배열에도 작성해줘야함**

* HOOKS는 선언해주는 순서가 중요함.

> # HOOKS에 대한 자잘한 팁들

- HOOKS시리즈는 순서가 중요해서, 중간에 바뀌면 안됨  
  (조건문,반복문,함수안에 웬만하면 넣지 않기) : **HOOKS는 무조건 최상위**

* **다른 HOOKS 안에 HOOKS 를 넣지도 않기**

* 정리  
  input배열이 바뀌기 전까지,
  **useMemo : 함수의 리턴값을 저장**  
  **useCallback : 함수를 저장**  
  input배열이 바뀔때,  
  **useEffect** : 안에 있는 명령문을 실행한다. 여러번 생성(작성)해도 된다.

  useState,useRef,useEffect,useMemo,useCallback,useReducer,useContext ....

* ### useEffect에서 didMount가아닌 didUpdate만 사용하고 싶다면,
  **작동을 막을순 없지만** didMount 작동에서는 아무것도 실행 안하게 하면 된다.  
  :

**useEffect에서 didUpdate 사용하기.**  
패턴(외우고 사용할것)

```
const mounted =useRef(false);
useEffect(()=>{
if(!mounted.current){
mounted.current =true;
} else {

}
//ajax 요청
},[바뀌는 값]); //componentDidUpdate만 작동하고 componentDidMount는 작동 안함
```
