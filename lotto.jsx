import React, { Component } from "react";

function getWinNumbers() {
  // 7개의 숫자를 미리 뽑음
  console.log("getWinNumbers"); // 콘솔로그의 이유 : 이 함수가 계산량이 많은 함수라, 반복실행을 막기위해
  const candidate = Array(45)
    .fill()
    .map((v, i) => i + 1);
  const shuffle = [];
  while (candidate.length > 0) {
    shuffle.push(
      candidate.splice(Math.floor(Math.random() * candidate.length), 1)[0]
    );
  }
  const bonusNumber = shuffle[shuffle.length - 1];
  const winNumbers = shuffle.slice(0, 6).sort((p, c) => p - c);
  return [...winNumbers, bonusNumber];
}

export default class lotto extends Component {
  state = {
    // 재실행 하기 위한 state
    winNumber: getWinNumbers(), // 숫자를 미리 뽑아둠
    winBalls: [], //배열안에 숫자가 들어가면 render부분이 작동
    bonus: null, // 보너스 공
    redo: false,
  };
  render() {
    const { winBalls, bonus, redo } = this.state;
    return (
      <>
        <div>당첨 숫자</div>
        <div id="결과창">
          {/* winBalls에 하나 씩 넣으면 map 메서드를 통해 Ball Jsx가 생성 */}
          {winBalls.map(v => (
            // 반복문을 기점으로 컴포넌트 분리화 한다.
            <Ball key={v} number={v} />
          ))}
        </div>
        <div>보너스!</div>
        {bonus && <Ball number={bonus} />}
        {redo && (
          <button onClick={redo ? this.onClickRedo : () => {}}>
            한 번 더 !
          </button>
        )}
      </>
    );
  }
}
