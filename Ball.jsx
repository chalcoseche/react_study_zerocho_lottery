import React, { PureComponent, memo } from "react"; // 보통 최하단 자식 컴포넌트는 PureComponent로 해두된다. (화면역할 하기 때문)

//함수컴포넌트로제작 (state를 안쓰고 , 데이터 계산이 없기 때문)
const BallFunction = memo(({ number }) => {
  //컴포넌트를 다른 컴포넌트로 감싸는것 = 하이오더 컴포넌트 = HOC
  // memo는 class 컴포넌트의 PureComponent역할을 함.
  let background;
  if (number <= 10) {
    background = "red";
  } else if (number <= 20) {
    background = "orange";
  } else if (number <= 30) {
    background = "yellow";
  } else if (number <= 40) {
    background = "blue";
  } else {
    background = "green";
  }
  return (
    <div className="ball" style={{ background }}>
      {number}
    </div>
  );
});

// class 컴포넌트
//
// export default class Ball extends PureComponent {
//   render() {
//     const { number } = this.props;
//     let background;
//     if (number <= 10) {
//       background = 'red';
//     } else if (number <= 20) {
//       background = 'orange';
//     } else if (number <= 30) {
//       background = 'yellow';
//     } else if (number <= 40) {
//       background = 'blue';
//     } else {
//       background = 'green';
//     }
//     return (
//       <div className="ball" style={{ background }}>
//         {number}
//       </div>
//     );
//   }
// }
